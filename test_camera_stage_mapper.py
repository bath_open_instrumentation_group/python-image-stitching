# -*- coding: utf-8 -*-
"""
Created on Mon Jun 08 15:10:39 2015

@author: Hera
"""

from nplab.instrument.camera.lumenera import LumeneraCamera
from nplab.instrument.stage import camera_stage_mapper
from nplab.instrument.stage.prior import ProScan
import numpy as np
import matplotlib
import matplotlib.pyplot as plt
import nplab

stage = ProScan('COM4')
stage.query("SERVO 1") #set up stage to use servocontrol
stage.query("UPR Z 500") #500 um per revolution = lab 6
stage.query("RES Z 0.02") #500 um per revolution = lab 6
camera = LumeneraCamera(1)
mapper = camera_stage_mapper.CameraStageMapper(camera, stage)

camera.live_view = True
camera.edit_traits()
mapper.edit_traits()

#mapper.acquire_tiled_image((3,3),live_plot=True)

def closeall():
    camera.live_view=False
    camera.close()
    stage.close()
    nplab.current_datafile().close()
    
#closeall()