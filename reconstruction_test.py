# -*- coding: utf-8 -*-
"""
Reconstruction of tiled images

@author: rwb27
"""

import nplab
import numpy as np
import matplotlib.pyplot as plt
import cv2
import cv2.cv
from scipy import ndimage

nplab.datafile.set_current("2015-10-01_3.h5",mode="r")
plot_as_we_go = True

df = nplab.current_datafile()

tiled_image = df['CameraStageMapper/tiled_image_2']
tiles = tiled_image.numbered_items("tile")
camera_to_sample = tiled_image.attrs.get("camera_to_sample")
image_size = np.array(tiles[0].shape[:2])

positions = []
for tile in tiles:
    positions.append(tile.attrs.get("camera_centre_position"))
positions = np.array(positions)

centre = np.mean(positions, axis=0)
pixel_positions = np.dot(positions[:,:2] - centre[:2], 
                         np.linalg.inv(camera_to_sample)) * image_size

if plot_as_we_go:
    downsample=10
    f, ax = plt.subplots(1,1)
    ax.set_aspect(1)
    #plot the images in position
    for pos, image in zip(pixel_positions, tiles):
        small_image = image[::downsample,::downsample,:]
        ax.imshow(small_image[::1,::-1,:].transpose(1,0,2),
                  extent=[pos[i] + s*image_size[i] 
                          for i in [0,1] for s in [-0.5,0.5]],
                 )
    #plot the outlines
    square = np.array([[-1,-1],[-1,1],[1,1],[1,-1],[-1,-1]])
    for pos in pixel_positions:
        rect = pos + image_size * 0.5 * square
        ax.plot(rect[:,0],rect[:,1])
    ax.autoscale()

#################### Identify overlapping pairs of tiles ######################
tile_pairs = []
for i in range(len(tiles)):
    for j in range(i+1, len(tiles)):
        overlap = image_size - np.abs(pixel_positions[i,:2] -
                                      pixel_positions[j,:2])
        overlap[overlap<0] = 0
        tile_pairs.append((i, j, np.product(overlap)))
overlaps = np.array([o for i,j,o in tile_pairs])
overlap_threshold = np.max(overlaps)*0.2
overlapping_pairs = [(i,j,o) for i,j,o in tile_pairs if o>overlap_threshold]
pixel_displacements = [pixel_positions[j,:2] - pixel_positions[i,:2]
                       for i,j,o in overlapping_pairs]

#################### Cross-correlate overlapping pairs to match them up #######
#FIXME currently half a pixel out for odd-sized images
#FIXME currently breaks if overlap in X or Y is 
margin = np.int(np.min(image_size) * 0.02)
correlated_pixel_displacements = []
for i, j, o in overlapping_pairs:
    original_displacement = np.round(pixel_positions[j,:2] - pixel_positions[i,:2])
    overlap_size = image_size - np.abs(original_displacement)
    assert np.all(overlap_size > margin), "Overlaps must be greater than the margin"
    assert np.all(overlap_size <= image_size), "Overlaps can't be bigger than the image"
    #this slightly dense structure creates slices for the overlapping part
    i_slices = [slice(0,ol) if od<0 else slice(im-ol,im) 
                for im, ol, od in zip(image_size.astype(np.int), 
                                      overlap_size.astype(np.int), 
                                      original_displacement)]
    j_slices = [slice(margin,ol-margin) if od>0 else slice(im-ol+margin,im-margin) 
                for im, ol, od in zip(image_size.astype(np.int), 
                                      overlap_size.astype(np.int), 
                                      original_displacement)]
    #correlate them: NB the match position is the MINIMUM
    corr = -cv2.matchTemplate(np.array(tiles[i])[i_slices+[slice(None)]],
                              np.array(tiles[j])[j_slices+[slice(None)]],
                              cv2.TM_SQDIFF_NORMED)
    corr += (corr.max()-corr.min())*0.1 - corr.max() #background-subtract 90% of maximum
    corr = cv2.threshold(corr, 0, 0, cv2.THRESH_TOZERO)[1] #zero out any negative pixels - but there should always be > 0 nonzero pixels
    peak = ndimage.measurements.center_of_mass(corr) #take the centroid (NB this is of grayscale values not just binary)
    shift = peak - np.array(corr.shape)/2
    correlated_pixel_displacements.append(original_displacement + shift)
correlated_pixel_displacements = np.array(correlated_pixel_displacements)
pixel_displacements = np.array(pixel_displacements)

print "Starting RMS Error: %.2f pixels" % np.std(pixel_displacements - 
                                            correlated_pixel_displacements)
                                            
####################### Fit an affine transform to fine-tune positions ########
affine_transform = np.linalg.lstsq(pixel_displacements, 
                                   correlated_pixel_displacements)[0]
corrected_pixel_displacements = np.dot(pixel_displacements, affine_transform)
corrected_pixel_positions = np.dot(pixel_positions, affine_transform)

corrected_camera_to_sample = np.dot(np.linalg.inv(affine_transform),
                                    camera_to_sample)

print "Correcting transformation matrix with affine transform: {0}".format(affine_transform)

def rms_error(print_err=False):
    error = np.std(corrected_pixel_displacements - 
                   correlated_pixel_displacements)
    if print_err:
        print "RMS Error: %.2f pixels" % error
    return error
        
e = rms_error(True)

if plot_as_we_go:
    f, ax = plt.subplots(1,1)
    ax.plot(correlated_pixel_displacements[:,0],correlated_pixel_displacements[:,1],'bo')
    ax.plot(corrected_pixel_displacements[:,0],corrected_pixel_displacements[:,1],'r+')

pairs = [(i,j) for (i,j,o) in overlapping_pairs]
###################### Look at each image and shift it to the optimal place ###
# this step can be re-run lots of times
def update_corrected_displacements():
    global corrected_pixel_displacements, pairs, corrected_pixel_positions
    global correlated_pixel_displacements
    corrected_pixel_displacements = np.array([corrected_pixel_positions[j] - 
                                        corrected_pixel_positions[i,:]
                                        for i,j in pairs])
    
def optimise_positions():
    global corrected_pixel_displacements, pairs, corrected_pixel_positions
    global correlated_pixel_displacements
    update_corrected_displacements()
    for k in range(len(tiles)):
        # find the measured and current displacements of overlapping images
        measured_d = np.array([d if i==k else -d 
                               for (i,j), d in zip(pairs, correlated_pixel_displacements)
                               if i==k or j==k])
        current_d = np.array([d if i==k else -d 
                               for (i,j), d in zip(pairs, corrected_pixel_displacements)
                               if i==k or j==k])
        # shift the current tile in the direction suggested by measured 
        # displacements
        #print "shift:", np.mean(measured_d-current_d,axis=0)
        corrected_pixel_positions[k,:] -= np.mean(measured_d-current_d,axis=0)
    update_corrected_displacements()
        
errors = [rms_error()]
while len(errors) < 5 or (errors[-2] - errors[-1]) > 0.001:
    optimise_positions()
    errors.append(rms_error(True))
    
if plot_as_we_go:
    f, ax = plt.subplots(1,1)
    ax.set_aspect('auto')
    ax.plot(errors)
    ax.set_xlabel("Iteration")
    ax.set_ylabel("RMS error in image positions")
    

if plot_as_we_go:
    downsample=3
    f, ax = plt.subplots(1,1)
    ax.set_aspect(1)
    #plot the images in position
    for pos, image in zip(corrected_pixel_positions, tiles):
        small_image = image[::downsample,::downsample,:]
        ax.imshow(small_image[::1,::-1,:].transpose(1,0,2),
                  extent=[pos[i] + s*image_size[i] 
                          for i in [0,1] for s in [-0.5,0.5]]
                 )
    ax.autoscale()
    #plot the outlines
    square = np.array([[-1,-1],[-1,1],[1,1],[1,-1],[-1,-1]])
    for pos in pixel_positions:
        rect = pos + image_size * 0.5 * square
        ax.plot(rect[:,0],rect[:,1])
    ax.autoscale()

if plot_as_we_go:
    downsample=3
    f = plt.figure()
    ax0 = f.add_subplot(1,2,1)
    ax1 = f.add_subplot(1,2,2,sharex=ax0,sharey=ax0)
    for ax in [ax0,ax1]:
        ax.set_aspect(1)
    #plot the images in position
    for ax, positions in [(ax0,pixel_positions),
                          (ax1,corrected_pixel_positions)]:
        for pos, image in zip(positions, tiles):
            small_image = image[::downsample,::downsample,:]
            ax.imshow(small_image[::1,::-1,:].transpose(1,0,2),
                      extent=[pos[i] + s*image_size[i] 
                              for i in [0,1] for s in [-0.5,0.5]]
                     )
        ax.autoscale()
        #plot the outlines
        square = np.array([[-1,-1],[-1,1],[1,1],[1,-1],[-1,-1]])
        for pos in positions:
            rect = pos + image_size * 0.5 * square
            ax.plot(rect[:,0],rect[:,1])
        ax.autoscale()

################### Stitch the images together ################################
positions = corrected_pixel_positions.copy()
downsample = 1
feather = 10
p = positions
stitched_size = np.round((np.max(p, axis=0) - np.min(p, axis=0) + image_size)/downsample)
stitched_centre = (np.max(p, axis=0) + np.min(p, axis=0))/2
stitched_image = np.zeros(tuple(stitched_size)+(3,), dtype=np.uint8)

if False:
    for i in range(stitched_image.shape[0]):
        for j in range(stitched_image.shape[1]):
            here = np.array([i,j]) * downsample - np.array(stitched_image.shape[:2])*downsample/2
            distances_squared = np.sum((positions - here)**2, axis=1)
            d_min = np.sqrt(np.min(distances_squared))
            tile_indices = np.where(distances_squared < (d_min + feather)**2)[0]
            weights = [d_min + feather - np.sqrt(distances_squared[k]) for k in tile_indices]
            weights /= np.sum(weights)
            for k, w in zip(tile_indices, weights):
                stitched_image[i, j, :] += w * np.array([[255,0,0],[0,255,0],[0,0,255]][k%3])

stitched_centres = (positions - stitched_centre 
                    + np.array(stitched_image.shape[:2])*downsample/2.0)/downsample
for i in range(len(tiles)):
    tile = tiles[i]
    centre = stitched_centres[i,:]
    topleft = centre - image_size/2.0/downsample
    #topleft is the (downsampled) pixel coordinates of the tile's corner in the big image
    shift = (np.ceil(topleft) - topleft) * downsample
    w, h, d = tile.shape
    img = np.zeros((w/downsample - 1, h/downsample - 1, d), dtype=np.uint8)
    #Crudely downsample (take a mean eventually)
    #for dx in range(downsample):
    #    for dy in range(downsample):
    #        img += tile[shift+dx:shift+dx+downsample*img.shape[0]:downsample,
    #                    shift+dy:shift+dy+downsample*img.shape[1]:downsample,
    #                    :] / downsample**2
    img = tile[int(shift[0]):int(shift[1]+downsample*img.shape[0]):downsample,
               int(shift[1]):int(shift[1]+downsample*img.shape[1]):downsample,
               :]
    for j in range(stitched_centres.shape[0]):
        other_centre = stitched_centres[j,:]
        if i != j: #don't compare to the current image
            difference = other_centre - centre
            midpoint = (other_centre + centre)/2
            for yi in range(img.shape[1]):
                y = np.ceil(topleft[1]) + yi
                xi_threshold = np.ceil(midpoint[0] 
                                        - (y - midpoint[1])*difference[1]/difference[0] 
                                        - np.ceil(topleft[0]))
                if difference[0] > 0:
                    if xi_threshold < 0:
                        xi_threshold = 0
                    if xi_threshold < img.shape[0]:
                        img[xi_threshold:,yi,:] = 0
                else:
                    if xi_threshold > img.shape[0]:
                        xi_threshold = img.shape[0]
                    if xi_threshold > 0:
                        img[:xi_threshold,yi,:] = 0
    print "Inserting image {0}/{1} at {2}, size {3}, canvas size {4}".format(
            i, len(tiles), np.ceil(topleft), img.shape, stitched_image.shape)
    stitched_image[np.ceil(topleft[0]):np.ceil(topleft[0])+img.shape[0],
                   np.ceil(topleft[1]):np.ceil(topleft[1])+img.shape[1],:] += img
f, ax = plt.subplots(1,1)
ax.imshow(stitched_image.transpose(0,1,2))
ax.plot(stitched_centres[:,1],stitched_centres[:,0],'ro')
    
apos = np.copy(positions)
apos -= stitched_centre
apos += np.array(stitched_image.shape[:2])/2
apos /= downsample

plt.imshow(stitched_image)
plt.plot(apos[:,0],apos[:,1],'bo')