# python-image-stitching

Scripts to stitch images together, preserving metadata as much as possible.

The origin of this script lies in particle finding experiments I did while working in Jeremy Baumberg's lab, which is why it depends on the `nplab` library.  

## Installation
I'm sorry that it's not (yet) nicely packaged with well-enumerated dependencies.  More or less all the dependencies come via ``nplab``, so see the [nplab repository] for installation instructions, which I hope include dependencies (!).  From memory, you will need at least:
* Python 2 (``nplab`` is not Py3 compatible yet)
* ``nplab`` (clone the repository and use ``python setup.py develop`` or ``python setup.py install``)
* ``opencv-python``
* ``qtpy`` (and a working set of Qt bindings - pyQt4 or 5 works I think)
* ``h5py``
* ``colorama`` (required by logging)
In case it helps, when I run ``pip freeze`` in my virtual environment for this, I get:
```
backports-abc==0.5
backports.functools-lru-cache==1.5
backports.shutil-get-terminal-size==1.0.0
cloudpickle==1.2.1
colorama==0.4.1
cycler==0.10.0
decorator==4.4.0
enum34==1.1.6
futures==3.2.0
h5py==2.9.0
ipykernel==4.10.0
ipython==5.8.0
ipython-genutils==0.2.0
jupyter-client==5.2.4
jupyter-core==4.5.0
kiwisolver==1.1.0
matplotlib==2.2.4
networkx==2.2
-e git+https://github.com/nanophotonics/nplab.git@f04d63c3ae28b72c447f214848ae3e742e2bd7d5#egg=nplab
numpy==1.16.4
opencv-python==4.1.0.25
pathlib2==2.3.4
pexpect==4.7.0
pickleshare==0.7.5
Pillow==6.0.0
prompt-toolkit==1.0.16
ptyprocess==0.6.0
Pygments==2.4.2
pyparsing==2.4.0
python-dateutil==2.8.0
pytz==2019.1
PyWavelets==1.0.3
PyYAML==5.1.1
pyzmq==18.0.2
QtPy==1.8.0
scandir==1.10.0
scikit-image==0.14.3
scipy==1.2.2
simplegeneric==0.8.1
singledispatch==3.4.0.3
six==1.12.0
subprocess32==3.5.4
tornado==5.1.1
traitlets==4.3.2
wcwidth==0.1.7
```
Not all of these will be required, of course!  In particular, I installed an ipython kernel in here for my convenience, and that's responsible for a good fraction of the requirements.

This code should be installed by cloning the repository.  It's not yet installable as a module.  It uses git-LFS to manage large data files, so you should make sure you have installed and enabled git-lfs.  This should make it easier to avoid accidentally downloading ~300Mb of example data if you just want the python scripts, though I think it will download everything by default.

## Usage
Run ``python reconstruct_tiled_image.py`` to stitch the example dataset.  Further usage probably requires reading the comments in that file.

NB there is a copy of the above script, that probably doesn't run (written with out of date numpy) and may or may not contain useful additions!


[nplab repository]: https://github.com/nanophotonics/nplab